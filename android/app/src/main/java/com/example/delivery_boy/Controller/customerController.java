package com.example.delivery_boy.Controller;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.delivery_boy.MainActivity;
import com.example.delivery_boy.Model.Location;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class customerController {
    public List<Location> findAll(Context c){
        List<Location> locations = new ArrayList<Location>();
        String URL="https://api.jsonbin.io/b/5f324f106f8e4e3faf301f00/3";
        RequestQueue requestQueue= Volley.newRequestQueue(c);

        JsonObjectRequest objectRequest=new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        requestQueue.add(objectRequest);
        return locations;
    }
}
