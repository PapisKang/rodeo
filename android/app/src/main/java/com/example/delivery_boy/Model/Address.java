package com.example.delivery_boy.Model;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.UUID;

public class Address {
    UUID id;
    String type;
    int postal_code;
    String city;
    String country;
    String address;
    String address2;
    BigDecimal longitude;
    BigDecimal latitude;
    Date createdAt;
    Date updatedAt;


    public Address(UUID id, String type, int postal_code, String city, String country, String address, String address2, BigDecimal longitude, BigDecimal latitude, Date createdAt, Date updatedAt) {
        this.id = id;
        this.type = type;
        this.postal_code = postal_code;
        this.city = city;
        this.country = country;
        this.address = address;
        this.address2 = address2;
        this.longitude = longitude;
        this.latitude = latitude;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(int postal_code) {
        this.postal_code = postal_code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
