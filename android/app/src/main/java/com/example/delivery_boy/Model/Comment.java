package com.example.delivery_boy.Model;

import java.sql.Date;
import java.util.UUID;

public class Comment {
    UUID id;
    Deliverer deliverer;
    Customer customer;
    String comment;
    float rating;
    Date date;
    Date createdAt;
    Date updatedAt;

    public Comment(UUID id, Deliverer deliverer, Customer customer, String comment, float rating, Date date, Date createdAt, Date updatedAt) {
        this.id = id;
        this.deliverer = deliverer;
        this.customer = customer;
        this.comment = comment;
        this.rating = rating;
        this.date = date;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Deliverer getDeliverer() {
        return deliverer;
    }

    public void setDeliverer(Deliverer deliverer) {
        this.deliverer = deliverer;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
