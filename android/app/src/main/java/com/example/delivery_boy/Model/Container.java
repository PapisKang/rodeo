package com.example.delivery_boy.Model;

import java.sql.Date;
import java.util.UUID;

public class Container {
    UUID id;
    String type;
    float capacity;
    String description;
    boolean active;
    Deliverer deliverer;
    Date createdAt;
    Date updatedAt;

    public Container(UUID id, String type, float capacity, String description, boolean active, Deliverer deliverer, Date createdAt, Date updatedAt) {
        this.id = id;
        this.type = type;
        this.capacity = capacity;
        this.description = description;
        this.active = active;
        this.deliverer = deliverer;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getCapacity() {
        return capacity;
    }

    public void setCapacity(float capacity) {
        this.capacity = capacity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Deliverer getDeliverer() {
        return deliverer;
    }

    public void setDeliverer(Deliverer deliverer) {
        this.deliverer = deliverer;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
