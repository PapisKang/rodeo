package com.example.delivery_boy.Model;

import java.sql.Date;
import java.util.UUID;

public class Delivererdetail {
    UUID id;
    int duration;
    float distance;
    float earning;
    Deliverer deliverer;
    Date createdAt;
    Date updatedAt;

    public Delivererdetail(UUID id, int duration, float distance, float earning, Deliverer deliverer, Date createdAt, Date updatedAt) {
        this.id = id;
        this.duration = duration;
        this.distance = distance;
        this.earning = earning;
        this.deliverer = deliverer;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public float getEarning() {
        return earning;
    }

    public void setEarning(float earning) {
        this.earning = earning;
    }

    public Deliverer getDeliverer() {
        return deliverer;
    }

    public void setDeliverer(Deliverer deliverer) {
        this.deliverer = deliverer;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
