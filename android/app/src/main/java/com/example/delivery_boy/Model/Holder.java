package com.example.delivery_boy.Model;

import java.sql.Date;
import java.util.UUID;

public class Holder {
    UUID id;
    String holder_id;
    String name;
    String web_url;
    Date createdAt;
    Date updatedAt;

    public Holder(UUID id, String holder_id, String name, String web_url, Date createdAt, Date updatedAt) {
        this.id = id;
        this.holder_id = holder_id;
        this.name = name;
        this.web_url = web_url;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getHolder_id() {
        return holder_id;
    }

    public void setHolder_id(String holder_id) {
        this.holder_id = holder_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWeb_url() {
        return web_url;
    }

    public void setWeb_url(String web_url) {
        this.web_url = web_url;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
