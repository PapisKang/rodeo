package com.example.delivery_boy.Model;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.UUID;

public class Location {
    UUID id;
    BigDecimal longitude;
    BigDecimal latitude;
    Date createdAt;
    Date updatedAt;

    public Location(UUID id, BigDecimal longitude, BigDecimal latitude, Date createdAt, Date updatedAt) {
        this.id = id;
        this.longitude = longitude;
        this.latitude = latitude;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
