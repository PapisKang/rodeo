package com.example.delivery_boy.Model;

import java.sql.Date;
import java.sql.Time;
import java.util.UUID;

public class Opening_hour {
    UUID id;
    int day;
    Time opentime;
    Time closetime;
    Date createdAt;
    Date updatedAt;

    public Opening_hour(UUID id, int day, Time opentime, Time closetime, Date createdAt, Date updatedAt) {
        this.id = id;
        this.day = day;
        this.opentime = opentime;
        this.closetime = closetime;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public Time getOpentime() {
        return opentime;
    }

    public void setOpentime(Time opentime) {
        this.opentime = opentime;
    }

    public Time getClosetime() {
        return closetime;
    }

    public void setClosetime(Time closetime) {
        this.closetime = closetime;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
