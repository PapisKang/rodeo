package com.example.delivery_boy.Model;

import java.sql.Date;
import java.util.List;
import java.util.UUID;

public class Order {
    UUID id;
    String tracking_number;
    String order_url;
    Date order_date;
    String order_status;
    Date estimated_arrival;
    String delivery_zone;
    String delivery_type;
    float delivery_fee;
    String delivery_note;
    float service_fee;
    float special_discount;
    float amount_charged;
    List<Event> events;
    Customer customer;
    Holder holder;
    Deliverer deliverer;
    Shop shop;
    Date createdAt;
    Date updatedAt;

    public Order(UUID id, String tracking_number, String order_url, Date order_date, String order_status, Date estimated_arrival, String delivery_zone, String delivery_type, float delivery_fee, String delivery_note, float service_fee, float special_discount, float amount_charged, List<Event> events, Customer customer, Holder holder, Deliverer deliverer, Shop shop, Date createdAt, Date updatedAt) {
        this.id = id;
        this.tracking_number = tracking_number;
        this.order_url = order_url;
        this.order_date = order_date;
        this.order_status = order_status;
        this.estimated_arrival = estimated_arrival;
        this.delivery_zone = delivery_zone;
        this.delivery_type = delivery_type;
        this.delivery_fee = delivery_fee;
        this.delivery_note = delivery_note;
        this.service_fee = service_fee;
        this.special_discount = special_discount;
        this.amount_charged = amount_charged;
        this.events = events;
        this.customer = customer;
        this.holder = holder;
        this.deliverer = deliverer;
        this.shop = shop;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTracking_number() {
        return tracking_number;
    }

    public void setTracking_number(String tracking_number) {
        this.tracking_number = tracking_number;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public String getOrder_url() {
        return order_url;
    }

    public void setOrder_url(String order_url) {
        this.order_url = order_url;
    }

    public Date getOrder_date() {
        return order_date;
    }

    public void setOrder_date(Date order_date) {
        this.order_date = order_date;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public Date getEstimated_arrival() {
        return estimated_arrival;
    }

    public void setEstimated_arrival(Date estimated_arrival) {
        this.estimated_arrival = estimated_arrival;
    }

    public String getDelivery_zone() {
        return delivery_zone;
    }

    public void setDelivery_zone(String delivery_zone) {
        this.delivery_zone = delivery_zone;
    }

    public String getDelivery_type() {
        return delivery_type;
    }

    public void setDelivery_type(String delivery_type) {
        this.delivery_type = delivery_type;
    }

    public float getDelivery_fee() {
        return delivery_fee;
    }

    public void setDelivery_fee(float delivery_fee) {
        this.delivery_fee = delivery_fee;
    }

    public String getDelivery_note() {
        return delivery_note;
    }

    public void setDelivery_note(String delivery_note) {
        this.delivery_note = delivery_note;
    }

    public float getService_fee() {
        return service_fee;
    }

    public void setService_fee(float service_fee) {
        this.service_fee = service_fee;
    }

    public float getSpecial_discount() {
        return special_discount;
    }

    public void setSpecial_discount(float special_discount) {
        this.special_discount = special_discount;
    }

    public float getAmount_charged() {
        return amount_charged;
    }

    public void setAmount_charged(float amount_charged) {
        this.amount_charged = amount_charged;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Holder getHolder() {
        return holder;
    }

    public void setHolder(Holder holder) {
        this.holder = holder;
    }

    public Deliverer getDeliverer() {
        return deliverer;
    }

    public void setDeliverer(Deliverer deliverer) {
        this.deliverer = deliverer;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
