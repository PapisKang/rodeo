package com.example.delivery_boy.Model;

import java.sql.Date;
import java.util.List;
import java.util.UUID;

public class Shop {
    UUID id;
    int avg_prep_time;
    String phone;
    String name;
    String reference;
    Address address;
    List<Opening_hour> opening_hours;
    Date createdAt;
    Date updatedAt;

    public Shop(UUID id, int avg_prep_time, String phone, String name, String reference, Address address, List<Opening_hour> opening_hours, Date createdAt, Date updatedAt) {
        this.id = id;
        this.avg_prep_time = avg_prep_time;
        this.phone = phone;
        this.name = name;
        this.reference = reference;
        this.address = address;
        this.opening_hours = opening_hours;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getAvg_prep_time() {
        return avg_prep_time;
    }

    public void setAvg_prep_time(int avg_prep_time) {
        this.avg_prep_time = avg_prep_time;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Opening_hour> getOpening_hours() {
        return opening_hours;
    }

    public void setOpening_hours(List<Opening_hour> opening_hours) {
        this.opening_hours = opening_hours;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
